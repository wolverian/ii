SHELL=/usr/bin/env bash

src=$(wildcard articles/*.md)
tmp=$(patsubst articles/%.md,tmp/articles/%.md,$(src))
tmp_html=$(tmp:.md=.html)
articles=$(patsubst articles/%.md,out/articles/%.html,$(src))

sed=sed '1d;5d'
lowdown=lowdown -T html -e metadata

.PHONY: usage
usage:
	@echo Usage:
	@echo
	@echo "make setup   – Install dependencies (macOS only)"
	@echo "make build   – Build site"
	@echo "make watch   – Watch and build on changes"
	@echo "make deploy  – Deploy site"
	@echo "make promote – Promote latest deployment to production"
	@echo "make clean   — Remove output and temporary state"

.PHONY: setup
setup:
	brew tap kristapsdz/repo
	brew install lowdown entr

.PHONY: build
build: out/index.html out/styles.css $(articles)

.PHONY: watch
watch:
	ls index.md styles.css articles/*.md | entr make build

.PHONY: deploy
deploy: build
	now

.PHONY: promote
promote:
	now alias

.PHONY: clean
clean:
	rm -f {tmp,out}/{articles/,}*.{html,md}

# Front page

out/index.html: tmp/index.html tmp/articles.html
	./commands/front-page > "$@"

# Styles

out/styles.css: styles.css
	cp "$<" "$@"

# Generic rules

out/articles/%.html: tmp/articles/%.md
	./commands/article-page "$<" > "$@"

# Temporary files

tmp/articles/%.md: articles/%.md
	$(sed) "$<" > "$@"

tmp/articles/%.html: tmp/articles/%.md
	./commands/article-fragment "$<" > "$@"

tmp/articles.list: $(tmp)
	./commands/article-list $(tmp) > "$@"

tmp/articles.html: tmp/articles.list $(tmp_html)
	cat $$(sed 's/md$$/html/' tmp/articles.list) > "$@"

tmp/index.html: index.md
	$(sed) "$<" | $(lowdown) -s - > "$@"
